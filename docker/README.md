# Tiamat Docker Images

**saltstack/tiamat** is a Docker container to ease compiling Python applications to binaries / exe files using tiamat.

## Tags

`saltstack/tiamat` Contains tags for building on several different architectures. `:latest` points to `:amd64`

The `:amd64` tag builds with a 64 bit version of GLIBC 2.6 based on cdrx/pyinstaller-linux

The `:i386` tag builds  with a 32 bit version of GLIBC 2.6 based on cdrx/pyinstaller-linux

The `:win64` tag builds uses wine to build 64 bit windows executables based on cdrx/pyinstaller-windows

The `:win32` tag builds uses wine to build 32 bit windows executables based on cdrx/pyinstaller-windows

## Usage

There are multiple containers, some for Linux and some for Windows builds. The Windows builder runs Wine inside Ubuntu to emulate Windows in Docker.

To build your application, you need to mount your source code into the `/src/` volume.

The application runs tiamat with the following base arguments: `tiamat build --directory /src`


```
docker run saltstack/tiamat --help
```

Running the application with `--help` shows all available options

```
docker run -v "/path/to/python/source:/src/" saltstack/tiamat:win64 -n project_name
```

will build your tiamat project into `/path/to/python/source/dist/project_name.exe`.

```
docker run -v "/path/to/python/source:/src/" saltstack/tiamat:amd64 -n project_name
```

will build your PyInstaller project into `/path/to/python/source/dist/project_name`.


## CICD

For cicd, simply set the environment variable "SRCDIR" to point to the directory with python
source code and run the application.

## License

Apache 2.0

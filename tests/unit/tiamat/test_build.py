from unittest import mock


def test_mk_requirements(hub, mock_hub, bname):
    mock_hub.tiamat.build.mk_requirements = hub.tiamat.build.mk_requirements
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].requirements = "requirements.txt"
    req_file = "test_req>=infinity"

    with mock.patch("builtins.open", mock.mock_open(read_data=req_file)) as mock_file:
        with mock.patch("os.path.exists", return_value=True):
            result = mock_hub.tiamat.build.mk_requirements(bname)
            mock_file.assert_called_with(result, "w+")


def test_builder(hub, mock_hub):
    mock_hub.tiamat.build.builder = hub.tiamat.build.builder
    name = "test_name"
    requirements = "test_reqs"
    sys_site = True
    exclude = {"test_exclude"}
    directory = "test_dir"
    bname = "test_build_name"

    mock_hub.tiamat.init.new.return_value = bname
    mock_hub.tiamat.build.builder(
        name,
        requirements,
        sys_site,
        exclude,
        directory,
    )
    mock_hub.tiamat.init.new.assert_called_once_with(
        name="test_name",
        requirements="test_reqs",
        sys_site=True,
        exclude={"test_exclude"},
        directory="test_dir",
        pyinstaller_version="4.3",
        pyinstaller_runtime_tmpdir=None,
        datas=None,
        build=None,
        pkg=None,
        onedir=False,
        pyenv="system",
        run="run.py",
        locale_utf8=False,
        dependencies=None,
        release=None,
        pkg_tgt=None,
        pkg_builder=None,
        srcdir=None,
        system_copy_in=None,
        python_bin=None,
        omit=None,
        venv_plugin=None,
        pyinstaller_args=[],
        timeout=300,
        pip_version="latest",
        venv_uninstall=[],
    )
    mock_hub.tiamat.virtualenv.init.create.assert_called_once_with(bname)
    mock_hub.tiamat.build.make.assert_called_once_with(bname)
    mock_hub.tiamat.virtualenv.init.scan.assert_called_once_with(bname)
    mock_hub.tiamat.virtualenv.init.mk_adds.assert_called_once_with(bname)
    mock_hub.tiamat.inst.mk_spec.assert_called_once_with(bname)
    mock_hub.tiamat.inst.call.assert_called_once_with(bname)
    mock_hub.tiamat.post.report.assert_called_once_with(bname)


def test_make(hub, mock_hub, bname):
    mock_hub.tiamat.build.make = hub.tiamat.build.make
    mock_hub.tiamat.BUILDS[bname].srcdir = "."
    conf = {"sources": [], "make": [], "src": [], "dest": []}
    mock_hub.tiamat.BUILDS[bname].build.proj = conf

    with mock.patch("shutil.copy"), mock.patch("shutil.copytree"):
        mock_hub.tiamat.build.make(bname)

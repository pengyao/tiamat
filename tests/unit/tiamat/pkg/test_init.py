from dict_tools import data


def test_build(hub, mock_hub, bname):
    mock_hub.tiamat.pkg.init.build = hub.tiamat.pkg.init.build
    mock_hub.OPT = data.NamespaceDict(tiamat=data.NamespaceDict(pkg_builder="fpm"))

    mock_hub.tiamat.pkg.init.build(bname)
    mock_hub.tiamat.pkg.fpm.build.assert_called_once_with(bname)

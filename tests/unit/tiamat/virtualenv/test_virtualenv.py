def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.virtualenv.bin = hub.tiamat.virtualenv.virtualenv.bin
    mock_hub.tiamat.virtualenv.virtualenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.virtualenv.create = (
        hub.tiamat.virtualenv.virtualenv.create
    )
    mock_hub.tiamat.virtualenv.virtualenv.create(bname)

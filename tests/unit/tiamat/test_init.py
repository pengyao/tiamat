from unittest import mock


def test_cli(hub, mock_hub):
    # Only mock the builder, everything else on the hub is real
    hub.tiamat.build.builder = mock_hub.tiamat.build.builder

    with mock.patch("os.path.abspath", return_value="test_dir"), mock.patch(
        "sys.argv",
        ["tiamat", "build", "-n", "pyproject", "--pyenv=3.9.0", "--timeout=500"],
    ), mock.patch("builtins.open"):
        hub.tiamat.init.cli()

    hub.tiamat.build.builder.assert_called_once_with(
        name="pyproject",
        requirements="requirements.txt",
        sys_site=False,
        exclude=None,
        directory="test_dir",
        pyinstaller_version="4.3",
        pyinstaller_runtime_tmpdir=None,
        venv_uninstall=(),
        datas=None,
        build=False,
        pkg={},
        onedir=False,
        pyenv="3.9.0",
        run="run.py",
        no_clean=False,
        locale_utf8=False,
        dependencies={},
        release={},
        pkg_tgt=None,
        pkg_builder="fpm",
        srcdir=None,
        system_copy_in=None,
        tgt_version=None,
        venv_plugin="builtin",
        python_bin=None,
        omit=None,
        pyinstaller_args=None,
        timeout="500",
        pip_version="latest",
    )


def test_new(hub, mock_hub, tempdir):
    mock_hub.tiamat.init.new = hub.tiamat.init.new
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.build.mk_requirements.return_value = "test_req"

    name = "test_name"
    requirements = "test_reqs"
    sys_site = True
    exclude = {"test_exclude"}
    directory = "test_dir"
    bname = "build_name"
    venv_dir = str(tempdir.mkdtemp())
    with mock.patch("uuid.uuid1", return_value=bname) as uu, mock.patch(
        "tempfile.mkdtemp", return_value=venv_dir
    ), mock.patch("os.path.abspath", return_value=directory):
        result = mock_hub.tiamat.init.new(
            name=name,
            requirements=requirements,
            sys_site=sys_site,
            exclude=exclude,
            directory=directory,
            pyinstaller_args=["--key=1234", "-v"],
            venv_plugin="builtin",
        )
        uu.assert_called_once_with()

    assert result == bname
    assert mock_hub.tiamat.BUILDS[bname] == {
        "all_paths": set(),
        "binaries": [],
        "build": None,
        "cmd": [
            f"{venv_dir}/bin/python3",
            "-B",
            "-O",
            "-m",
            "PyInstaller",
            "--exclude-module",
            "test_exclude",
        ],
        "datas": set(),
        "dependencies": None,
        "pyinstaller_version": "4.3",
        "dir": "test_dir",
        "exclude": {"test_exclude"},
        "imports": set(),
        "is_win": False,
        "locale_utf8": False,
        "name": "test_name",
        "omit": None,
        "onedir": False,
        "pkg": None,
        "pkg_builder": None,
        "pkg_tgt": None,
        "pybin": f"{venv_dir}/bin/python3",
        "pyenv": "system",
        "pyinstaller_runtime_tmpdir": None,
        "pypi_args": [
            f"{venv_dir}/bin/test_name",
            "--log-level=INFO",
            "--noconfirm",
            "--onefile",
            "--clean",
            "--key=1234",
            "-v",
        ],
        "release": None,
        "req": "test_req",
        "requirements": "test_dir/test_reqs",
        "run": "test_dir/run.py",
        "s_path": f"{venv_dir}/bin/test_name",
        "spec": "test_dir/test_name.spec",
        "srcdir": None,
        "sys_site": True,
        "system_copy_in": None,
        "venv_dir": venv_dir,
        "venv_plugin": "builtin",
        "venv_uninstall": None,
        "vroot": f"{venv_dir}/lib",
        "timeout": 300,
        "pip_version": "latest",
    }
